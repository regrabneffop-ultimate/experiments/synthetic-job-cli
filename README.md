# Synthetic Job CLI



Leveraging the [pipeline-status-of-a-commit](https://docs.gitlab.com/ee/api/commits.html#set-the-pipeline-status-of-a-commit) API Endpoint, allow users
to create synthetic jobs in a given pipeline.

https://docs.gitlab.com/ee/ci/variables/predefined_variables.html