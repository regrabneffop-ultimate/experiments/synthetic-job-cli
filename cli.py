import os
import requests

if os.path.isfile('.env'):
    try: 
        from envparse import Env        
        env = Env()
        env.read_envfile('.env')
    except ImportError:
        env = os.getenv



def get_commit_sha_from_pipeline_id(pipeline_id):
    """
    GET /projects/:id/pipelines/:pipeline_id
    """
    origin = env('CI_API_V4_URL')
    private_token = env('PRIVATE_TOKEN')
    project_id = env('PROJECT_ID')
    pipeline_id = env('PIPELINE_ID')
    
    # use the pipelines api endpoint to get the commit sha
    response = requests.get(f'{origin}/projects/{project_id}/pipelines/{pipeline_id}')
    commit_sha = response.json()['sha']
    return commit_sha




def main():
    """
    Provide ability to create jobs, given the Commit SHA.
    sjcli <commit_sha|pipeline_id> <job_name> <state:pending|running|success|failed|canceled> --target-url <url> --description <description> 

    POST /projects/:id/statuses/:sha
    """
    origin = env('CI_API_V4_URL')
    private_token = env('PRIVATE_TOKEN')
    project_id = env('PROJECT_ID')

    # Need to perform some logic to determine what the commit sha is based on the pipeline id
    commit_sha = env('COMMIT_SHA')
    if commit_sha.isnumeric()
        pipeline_id = env('COMMIT_SHA')
        commit_sha = get_commit_sha_from_pipeline_id(pipeline_id)
    
    ref = env('COMMIT_REF')
    
    job_name = env('JOB_NAME')
    status = env('STATUS')
    target_url = env('TARGET_URL')
    description = env('DESCRIPTION')

    requests.post(f'{origin}/projects/{project_id}/statuses/{commit_sha}?private_token={private_token}', json={
        'name': job_name,
        'state': state,
        'target_url': target_url,
        'description': description
    })
    print('Job created')




if __name__ == '__main__':
    main()
